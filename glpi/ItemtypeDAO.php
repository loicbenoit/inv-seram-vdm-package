<?php
namespace VdmPackage\services\glpi;

use \VdmPackage\services\helpers\UrlHelper;
use \DbUtils as DbUtils;
use \Toolbox as Logger;
use \CommonDBTM;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Generic model for getting and saving GLPI objects
 * Must be statefull unlike to other services, because of itemtype validation.
 *
 */
class ItemtypeDAO
{
   protected $supported_types = [];


   /*
    * Tell if the field is defined for the given itemtype or item
    *
    * @param   string         $field_name The field code name
    * @param   string/object  $item       The itemtype or item
    * @return  bool
    * @throws \Exception   when inputs are invalid
   */
   public static function isField(string $field_name, $item) {
      if( ! is_object($item)) {
         if( ! is_string($item)) {
            throw new \Exception(
               'Usage: Parameter "item" must be a class name or an object'
            );
         }
         $item = new $item();
         $item->getEmpty();
      }

      if( ! isset($item->fields) OR ! is_array($item->fields)) {
         throw new \Exception(
            'Usage: Item must have a property called "fields" of type array. See class CommonGLPI.'
         );
      }
      return array_key_exists($field_name, $item->fields);
   }

   /*
    * Set supported types
    *
    * @param   array   $types   An array of itemtype strings
    * @return  void
   */
   public function setSupportedTypes(array $types) {
      $this->supported_types = $types;
   }

   /*
    * Get supported types
    *
    * @return  array A list of strings
   */
   public function getSupportedTypes() {
      return $this->supported_types;
   }

   /*
    * Fetch a GLPI object by itemtype and ID
    *
    * @param   string   $itemtype
    * @param   int      $id
    * @return  object   A GLPI object or NULL
   */
   public function fetchItem(string $itemtype, $id) {
      if( ! $this->isSupported($itemtype) OR ! is_numeric($id)) {
         Logger::logDebug(
            'Itemtype not supported: '
            .var_export($itemtype, true)
            .' or invalid id: '
            .$id
         );
         $backtrace = debug_backtrace();
         Logger::logDebug(var_export($backtrace, true));
         return NULL;
      }

      //Instanciate and hydrate the object
      $item = new $itemtype();
      $item->getFromDB($id);
      return is_object($item) ? $item : NULL;
   }

   /*
    * Check if there is at least one record matching criteria for itemtype
    *
    * @param   string   $itemtype
    * @param   array    $where
    * @return  bool
   */
   public function existsWhere(string $itemtype, array $where) {
      if( ! $this->isSupported($itemtype) OR empty($where)) {
         return false;
      }
      global $DB;

      $item = new $itemtype();
      $item->getFromDBByCrit($where);

      $crit = ['SELECT' => 'id',
               'FROM'   => $item->getTable(),
               'WHERE'  => $where];

      $iter = $DB->request($crit);
      return count($iter) > 0;
   }


   /*
    * Update an item's data directly (no validation, no rule, no unicity check).
    *
    * @param CommonDBTM  $part       The part to update
    * @param array       $new_data   Map of fully qualified property to value
    * @return  void
   */
   public static function updateItemWithoutValidation(CommonDBTM $item, array $new_data = []) {
      //Cache old values for GLPI modification log
      $old_values = [];
      foreach($new_data as $k => $v) {
         if(isset($item->fields[$k])) {
            $old_values[$k] = $item->fields[$k];
         } else {
            Logger::logDebug('Unknown field: '.$k);
         }
      }

      //Update item with new values
      foreach($new_data as $k => $v) {
         if(isset($item->fields[$k])) {
            $item->fields[$k] = $v;
         }
      }

      //GLPI needs to be told which item fields where modified.
      $modified_fields = array_keys($old_values);

      //Apply update in DB and log modifications
      $item->updateInDB(
         $modified_fields,
         $old_values
      );
   }

   /*
    * Tell if given type is supported
    *
    * @param   string   $itemtype
    * @param   bool     $is_case_sensitive   (Optional) true
    * @return  object   A GLPI object or NULL
   */
   public function isSupported(string $itemtype, $is_case_sensitive = true) {
      if($is_case_sensitive) {
         $supported_types = $this->supported_types;
      } else {
         $itemtype = strtolower($itemtype);
         $supported_types = array_map('strtolower', $this->supported_types);
      }
      return in_array($itemtype, $supported_types, true);
   }

   /**
   * Get the URL of the item's page
   *
   * @param  object  $part A CommonDBTM object
   * @return  string An absolute URL or a relative URL that begins with a slash
   */
   public static function get_item_url(\CommonDBTM $item = NULL, $dft = '') {
      if( ! is_object($item)) {
         //Logger::logDebug('Item is not an object: '.var_export($item, true));
         return $dft;
      }
      $url = $item->getFormURLWithID($item->getID());

      //If relative, prepend slash.
      $url = UrlHelper::isAbsolute($url)
         ? $url
         : '/'.ltrim($url, '/');
      return $url;
   }


//-----------------------------------------------------------------------------
// Tests
//-----------------------------------------------------------------------------

   /**
   * Get the URL of the item's page
   *
   * @param  object  $part A CommonDBTM object
   * @return  string An absolute URL or a relative URL that begins with a slash
   */
   public static function test_existsWhere() {
      $data_exists = [
         'itemtype' => 'PluginGenericobjectPostefixe',
         'items_id' => 53,
         'tickets_id' => 95,
      ];

      $data_new = [
         'itemtype' => 'PluginItemtypeDoesNotExists',
         'items_id' => 1,
         'tickets_id' => 2,
      ];

      $itdao = new ItemtypeDAO();
      $itdao->setSupportedTypes(['Item_Ticket']);

      if( ! $itdao->existsWhere('Item_Ticket', $data_exists)) {
         throw new \Exception('Should exist: '.var_export($data_exists, true));
      }

      if($itdao->existsWhere('Item_Ticket', $data_new)) {
         throw new \Exception('Should NOT exist: '.var_export($data_new, true));
      }
   }

}
