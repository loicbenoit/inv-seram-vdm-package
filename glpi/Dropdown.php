<?php
namespace VdmPackage\services\glpi;

use VdmPackage\errors\PublicException;
use \Toolbox as Logger;
use \Dropdown as GlpiDropdown;

/**
 * Each entry represents a repair to an item associated to a vehicule.
 * This class wraps a GLPI object of CommonGLPI descent, usually an object
 * of class PluginGenericobjectVehicule.
 *
 */
class Dropdown {

   /**
    * Get the external table associated to a dropdown field.
    *
    * @param  string $property  The itemtype actual property name
    *
    * @return  string   The table name or an empty string
    * @throws  PublicException
    */
   public static function getTableNameFromPropertyName($property) {
      //Extract table name from actual itemtype property name.
      return 'glpi_'.preg_replace(
         '/_id$/',
         '',
         $property
      );
   }

   /**
    * Get a dropdown option label given the option's value.
    * Note: Convert an external key into it's textual representation.
    *
    * @param  string $table   The external table name
    * @param  string $key     The option value (the ID of the record in table)
    * @param  string $dft     The default value to return if not found
    *
    * @return  string
    * @throws  PublicException
    */
   public static function getOptionLabelFromKey($table, $key, $dft = '') {
      $retval = null;

      //Convert external key into text
      try {
         $retval = GlpiDropdown::getDropdownName(
            $table,
            $key
         );
      } catch(\Exception $e) {
         Logger::logDebug('Exceptio: '.$e->message());
      }

      //Decide if we use the converted value or not.
      //REM: GLPI returns '&nbsp;' when it can't find a value.
      //     User should't provide such values, so it's a clear error flag.
      return ($retval && is_string($retval) && $retval != '&nbsp;')
         ? $retval
         : $dft;
   }

   /**
    * Test the method
    *
    * @return  void
    * @TODO Perform these tests
    */
   public static function test_getTableNameFromPropertyName() {
      $properties = [
         'plugin_genericobject_modelradiomobiles_id',
         'plugin_genericobject_equipmenttyperadiomobiles_id',
      ];
      foreach($properties as $property) {
         echo '<pre>', self::getTableNameFromPropertyName($property), '</pre>';
      }
   }

   /**
    * Test the method
    *
    * @return  void
    * @TODO Perform these tests
    */
   public static function test_getOptionLabelFromKey() {
      $tests = [
         [
            'table' => 'plugin_genericobject_modelradiomobiles_id',
            'key' => 2, //Must actually exists in GLPI database
            'dft' => 'Dft value',
            'expected' => '', //Insert value from database
         ],
      ];
      foreach($tests as $test) {
         $retval = self::getOptionLabelFromKey(
            $test['table'],
            $test['key'],
            $test['dft']
         );
         if($test['expected'] !== $retval) {
            echo '<pre>';
               echo 'result: '.$retval;
               echo "\n";
               echo 'expected: '.$test['expected'];
            echo '</pre>';
         }
      }
   }

}
