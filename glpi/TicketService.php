<?php
namespace VdmPackage\services\glpi;

use VdmPackage\services\glpi\UserService;
use \CommonITILActor;
use \CommonITILValidation;
use \Exception;
use \Item_Ticket;
use \Item_Problem;
use \ITILSolution;
use \Problem;
use \Problem_Ticket;
use \Problem_User;
use \Ticket;
use \Toolbox as Logger;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Service for interacting with GLPI tickets
 *
 */
class TicketService
{
   CONST ROLE_REQUESTOR = CommonITILActor::REQUESTER;
   CONST ROLE_OBSERVER = CommonITILActor::OBSERVER;
   CONST ROLE_ASSIGNED = CommonITILActor::ASSIGN;

   /*
   * Create a ticket
   *
   * @param   string $title         Field name
   * @param   string $content       Field content (description)
   * @param   int    $category_id   Field itilcategories_id
   *                                See /glpi/front/itilcategory.php
   * @param   int    $status        Field status (7)
   *                                See Ticket::getAllStatusArray()
   *                                Based on CommonITILObject status constants
   * @param   int    $requestor_id  ID of the user requesting help
   * @param   int    $observer_id   ID of the user taking note of the request
   * @param   int    $assigned_id   ID of the user responsible for solving the
   *                                problem
   * @param   array  $other_data    (Optional) Other problem properties to save
   *                                See table glpi_problems
   *
   * @return   object   A problem instance
   * @throws \Exception when record creation fails
   */
   public static function createTicket(
      string $title,
      string $content,
      int $category_id,
      int $status,
      int $requestor_id = 0,
      int $observer_id = 0,
      int $assigned_id = 0,
      array $other_data = []
   ) {
      $ticket = new Ticket();

      $data = $other_data;
      $data['name'] = $title;
      $data['content'] = $content;
      $data['itilcategories_id'] = $category_id;
      $data['status'] = $status;

      if($requestor_id > 0) {
         $data['_users_id_requester'] = $requestor_id;
      }
      if($observer_id > 0) {
         $data['_users_id_observer'] = [$observer_id];
      }
      if($assigned_id > 0) {
         $data['_users_id_assign'] = $assigned_id;
      }

      $id = $ticket->add($data);
      if( ! is_numeric($id)) {
         throw new Exception('Failed to create ticket');
      }
      $ticket = new Ticket();
      $ticket->getFromDb($id);
      return $ticket;
   }

   /*
   * Create a ticket of type problem
   *
   * @param   string $title         Field name
   * @param   string $content       Field content (description)
   * @param   int    $category_id   Field itilcategories_id
   *                                See /glpi/front/itilcategory.php
   * @param   int    $status        Field status (7)
   *                                See CommonITILObject status constants
   * @param   int    $requestor_id  ID of the user requesting help
   * @param   int    $observer_id   ID of the user taking note of the request
   * @param   int    $assigned_id   ID of the user responsible for solving the
   *                                problem
   * @param   array  $other_data    (Optional) Other problem properties to save
   *                                See table glpi_problems
   *
   * @return   object   A problem instance
   * @throws \Exception when record creation fails
   */
   public static function createProblem(
      string $title,
      string $content,
      int $category_id,
      int $status,
      int $requestor_id = 0,
      int $observer_id = 0,
      int $assigned_id = 0,
      array $other_data = []
   ) {
      $problem = new Problem();

      $data = $other_data;
      $data['name'] = $title;
      $data['content'] = $content;
      $data['itilcategories_id'] = $category_id;
      $data['status'] = $status;

      if($requestor_id > 0) {
         $data['_users_id_requester'] = $requestor_id;
      }
      if($observer_id > 0) {
         $data['_users_id_observer'] = [$observer_id];
      }
      if($assigned_id > 0) {
         $data['_users_id_assign'] = $assigned_id;
      }

      $id = $problem->add($data);
      if( ! is_numeric($id)) {
         throw new Exception('Failed to create problem ticket');
      }
      $problem = new Problem();
      $problem->getFromDb($id);

      return $problem;
   }

   /*
   * Associate an item to a ticket.
   *
   * @param string   $itemtype
   * @param int   $item_id
   * @param int   $ticket_id
   *
   * @return   void
   * @throws \Exception when record creation fails
   */
   public static function assignItemToTicket(
      string $itemtype,
      int $item_id,
      int $ticket_id
   ) {
      $it = new Item_Ticket();

      $data = [];
      $data['itemtype'] = $itemtype;
      $data['items_id'] = $item_id;
      $data['tickets_id'] = $ticket_id;

      //Logger::logDebug('Will try to associate item to ticket:'.PHP_EOL.var_export($data, true));

      $itdao = new ItemtypeDAO();
      $itdao->setSupportedTypes(['Item_Ticket']);
      if( ! $itdao->existsWhere('Item_Ticket', $data)) {
         //Logger::logDebug('Must associate item to ticket');
         $id = $it->add($data);
         if( ! is_numeric($id)) {
            throw new Exception('Failed to associate item to ticket');
         }
      } else {
         //Logger::logDebug('Item already associated to ticket');
      }
   }

   /*
   * Associate an item to a ticket.
   *
   * @param string   $itemtype
   * @param int   $item_id
   * @param int   $ticket_id
   *
   * @return   void
   * @throws \Exception when record creation fails
   */
   public static function assignItemToProblem(
      string $itemtype,
      int $item_id,
      int $problem_id
   ) {
      $ip = new Item_Problem();

      $data = [];
      $data['itemtype'] = $itemtype;
      $data['items_id'] = $item_id;
      $data['problems_id'] = $problem_id;

      $itdao = new ItemtypeDAO();
      $itdao->setSupportedTypes(['Item_Problem']);
      if( ! $itdao->existsWhere('Item_Problem', $data)) {
         $id = $ip->add($data);
         if( ! is_numeric($id)) {
            throw new Exception('Failed to associate item to problem');
         }
      }
   }

   /*
   * Associate a problem to a ticket.
   *
   * @param int   $item_id
   * @param int   $ticket_id
   *
   * @return   void
   * @throws \Exception when record creation fails
   */
   public static function assignProblemToTicket(
      int $problem_id,
      int $ticket_id
   ) {
      $pt = new Problem_Ticket();

      $data = [];
      $data['problems_id'] = $problem_id;
      $data['tickets_id'] = $ticket_id;

      $itdao = new ItemtypeDAO();
      $itdao->setSupportedTypes(['Problem_Ticket']);
      if( ! $itdao->existsWhere('Problem_Ticket', $data)) {
         $id = $pt->add($data);
         if( ! is_numeric($id)) {
            throw new Exception('Failed to associate problem to ticket');
         }
      }
   }

   /*
   * Assign a user to a ticket, by role.
   * Users and problems are associated through table glpi_problems_users
   *
   * @param int   $problem_id       A problem id
   * @param int   $user_id          A user id
   * @param int   $role             A role code (see class constants)
   * @param int   $use_notification (Optional) True to send notification
   * @param int   $alternative_email   (Optional) Another notification email
   *
   * @return   int   The record ID
   * @throws \Exception when record creation fails
   */
   public static function assignUserToProblem(
      int $problem_id,
      int $user_id,
      int $role,
      int $use_notification = 1,
      string $alternative_email = ''

   ) {
      $pu = new Problem_User();
      $data = [];
      $data['problems_id'] = $problem_id;
      $data['users_id'] = $user_id;
      $data['type'] = $role;
      $data['use_notification'] = $use_notification;
      if(strlen($alternative_email)) {
         $data['alternative_email'] = $alternative_email;
      }

      $id = $pu->add($data);
      if( ! is_numeric($id)) {
         throw new Exception('Failed to associate a user to a problem ticket');
      }
      return $id;
   }

   /*
   * Create a solution for a ticket
   * REM: Can not exist without an associated ticket.
   *
   * @param   string $ticket_itemtype   The itemtype of the ticket (class name)
   *                                    For example: "Problem"
   * @param   int    $ticket_id     The ID of the associated ticket
   * @param   int    $category_id   Field solutiontypes_id
   *                                See /glpi/front/solutiontype.php
   * @param   string $content       Field content (description)
   * @param   int    $author_id     ID of the user who authored the solution
   * @param   array  $other_data    (Optional) Other solution properties to save
   *                                See table glpi_itilsolutions
   *
   * @return   object   A solution instance
   * @throws \Exception when record creation fails
   */
   public static function createSolution(
      string $ticket_itemtype,
      int $ticket_id,
      int $category_id,
      string $content,
      int $author_id,
      array $other_data = []
   ) {
      $solution = new ITILSolution();

      $data = $other_data;
      $data['itemtype'] = $ticket_itemtype;
      $data['items_id'] = $ticket_id;
      $data['solutiontypes_id'] = $category_id;
      $data['content'] = $content;
      $data['status'] = CommonITILValidation::ACCEPTED;

      if($author_id > 0) {
         if( ! isset($other_data['users_id'])) {
            $data['users_id'] = $author_id;
         }
         if( ! isset($other_data['users_id_approval'])) {
            $data['users_id_approval'] = $author_id;
         }
      }

      $id = $solution->add($data);
      if( ! is_numeric($id)) {
         throw new Exception(
            'Failed to create solution for ticket with id: '.$ticket_id
         );
      }
      $solution = new ITILSolution();
      $solution->getFromDb($id);
      return $solution;
   }


//-----------------------------------------------------------------------------
// Tests
//-----------------------------------------------------------------------------

   /*
   * Test the method
   *
   * This test will create 2 problem tickets:
   *  1- A problem ticket with status "Active"
   *  2- A problem with status "Assigned"
   */
   public static function test_createProblem() {
      $problem = \VdmPackage\services\glpi\TicketService::createProblem(
         'Foo title'.date('Y-m-d H:i:s'),
         'Foo content with status accepted created on '.date('Y-m-d H:i:s'),
         52, //Category: Demande de réparation
         \CommonITILObject::ACCEPTED, //Status
         134, //User: Dalanda
         10 //User: Loic
      );

      //Although the status is set to ACCEPTED, assigning the problem to a user
      //automatically corrects the status to ASSIGNED
      $problem = \VdmPackage\services\glpi\TicketService::createProblem(
         'Foo title'.date('Y-m-d H:i:s'),
         'Foo content with status assigned created on '.date('Y-m-d H:i:s'),
         52, //Category: Demande de réparation
         \CommonITILObject::ACCEPTED, //Status.
         134, //User: Dalanda
         10, //User: Loic
         157 //User: DemoDrmi
      );
   }
   /*
   * Test the method
   *
   * This test will create 2 problem tickets:
   *  1- A problem ticket with status "Active"
   *  2- A problem with status "Assigned"
   */
   public static function test_assignUserToProblem() {
      //Because requestor is not provided, GLPI will use the logged-in user.
      $problem_id = \VdmPackage\services\glpi\TicketService::createProblem(
         'Buzz title'.date('Y-m-d H:i:s'),
         'Buzz content test_assignUserToProblem created on '.date('Y-m-d H:i:s'),
         52, //Category: Demande de réparation
         \CommonITILObject::ACCEPTED //Status
      );

      //This will add a new requestor, but it won't remove previous ones
      self::assignUserToProblem(
         $problem_id,
         160,
         \VdmPackage\services\glpi\TicketService::ROLE_REQUESTOR
      );

      //This will assign a new oberver, but won't remove previous ones
      self::assignUserToProblem(
         $problem_id,
         134,
         \VdmPackage\services\glpi\TicketService::ROLE_OBSERVER
      );

      //This will assign a new assigned, but won't remove previous ones
      //This may also force a status change (see GLPI ticket lifecycle)
      self::assignUserToProblem(
         $problem_id,
         157,
         \VdmPackage\services\glpi\TicketService::ROLE_ASSIGNED
      );
   }


}
