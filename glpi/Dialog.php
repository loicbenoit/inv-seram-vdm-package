<?php
namespace VdmPackage\services\glpi;

use VdmPackage\services\helpers\UrlHelper;
use VdmPackage\services\helpers\ArrayHelper;
use VdmPackage\services\helpers\HtmlHelper;
//use \retl\system\libraries\Paths;
//use \retl\system\libraries\GenericHtmlConverter;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Service for creating dialog interfaces as layers or popup in a page.
 * Interface to document GLPI and decouple the plugin inner logic from GLPI.
 *
 */
class Dialog
{
   /**
    * Get the HTML of a button that opens a modal dialog window.
    * REM: May includes inline Javascript code.
    *
    * @param   string   $label         The button label
    * @param   string   $url           URL of the HTML to load in the dialog
    *                                  Note: Must not have query parameters.
    * @param   array    $qsmap         (Optional) A map for the query string
    *                                  Eg: host.com/foo/?key1:value1&key2:value2
    * @param   string   $attributes    (Optional) A map of button tag attributes
    * @param   string   $title         (Optional) The modal window title
    * @param   bool     $reload_on_close  (Optional) TRUE: Reload the
    *                                     host page when closing the modal
    *                                     to refresh content and CSRF token.
    *                                     Default: TRUE
    *
    * @return  string   The HTML of the button.
    */
   public static function createButtonForLargeModal(
      string $label,
      string $url,
      array $qsmap = [],
      array $attributes = [],
      string $title = '',
      bool $reload_on_close = true
   ) {
      $html = [];
      //$html[] = '<button type="button" class="vdmseram__preitemform__button" data-process="test1">Dialogue</button>';

      //Create an ID for the DOM element that will receive the modal window
      $domid = 'dialoglargemodal'.mt_rand();
      //$html[] = '<div id="'.$domid.'" class="hidden"></div>';

      //Can't overwrite this attribute, because it complicates value escaping.
      unset($attributes['onClick']);

      //Prepare the button tag attributes.
      $default_escapable_attributes = [
         'type' => 'button',
      ];
      $default_unescapable_attributes = [
         'onClick' => \Html::jsGetElementbyID($domid).".dialog('open');"
      ];
      $attributes_str = HtmlHelper::prepareTagAttributes(
         $attributes,
         $default_escapable_attributes,
         $default_unescapable_attributes
      );
/*
      $attributes = array_replace($default_escapable_attributes, $attributes);

      //Encode keys and values
      $attributes = ArrayHelper::encodeKeys($attributes, 'htmlspecialchars');
      $attributes = array_map('htmlspecialchars', $attributes);

      //Add unescapable attributes
      $attributes = array_replace($attributes, $default_unescapable_attributes);

      //Convert attribute map to string
      $attributes = ArrayHelper::implodeHtmlAttributeMap($attributes);
*/

      //Create the html of the button
      $html[] = '<button '.$attributes_str.' >';
         $html[] = htmlspecialchars($label);
      $html[] = '</button>';

      //Create javascript that will display the modal window
      $url = UrlHelper::appendQueryParameters($url, $qsmap);
      $html[] = \Ajax::createIframeModalWindow(
         $domid,
         $url,
         [
            'display'       => false,
            'width'         => 1024, //800,//'"80%"',
            'height'        => 600, //'"95%"',
            'reloadonclose' => $reload_on_close
         ]
      );

      return implode('', $html);
   }


}
