<?php
namespace VdmPackage\services\glpi;

use VdmPackage\services\helpers\UrlHelper;
use VdmPackage\services\helpers\ArrayHelper;
use VdmPackage\services\helpers\HtmlHelper;
//use \retl\system\libraries\Paths;
//use \retl\system\libraries\GenericHtmlConverter;

use \User as User;
use \Session as Session;
use \DbUtils as DbUtils;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Service for interacting with GLPI users
 *
 */
class UserService
{

   /**
    * Get the user object for given id.
    *
    * @param  int    $id   The user ID
    * @return  object/null
    */
   public static function getUser($id) {
      $user = new User();
      $result = false;
      if(is_numeric($id)) {
         $result = $user->getFromDB($id);
      }

      return $result && is_object($user) ? $user : NULL;
   }

   /**
    * Tell if the user id exists.
    *
    * @param  int    $id   The user ID
    * @return  bool
    */
   public static function exists($id) {
      $user = self::getUser($id);
      return is_object($user);
   }

   /**
    * Get the user that is currently logged-in or null.
    *
    * @return  object/null
    * @throws  PublicException
    */
   public static function getCurrent() {
      self::getCurrentId();
   }

   /**
    * Get the id of the user that is currently logged-in or null.
    *
    * @return  int/null
    * @throws  PublicException
    */
   public static function getCurrentId() {
      $retval = Session::getLoginUserID();
      return is_numeric($retval) ? $retval : null;
   }

   /**
    * Get the user name given a user id.
    *
    * @param  int    $id   The user ID
    * @return  string   The user name or an empty string
    */
   public static function getUserName($id) {
      $dbutils = new DbUtils();
      return $dbutils->getUserName($id, 0);
   }

   /**
    * Get the user name given a user id.
    *
    * @param  int    $id   The user ID
    * @return  string   The user login name or an empty string
    */
   public static function getUserLoginName($id) {
      $user = new User();
      if(is_numeric($id)) {
         $user->getFromDB($id);
      }
      return isset($user->fields['name']) && ! empty($user->fields['name'])
         ? $user->fields['name']
         : '';
   }

   /**
    * Get the user name given a user id as an anchor to the user page.
    *
    * @param  int    $id   The user ID
    * @return  string   The user name or an empty string
    */
   public static function getUserNameAnchor($id) {
      $dbutils = new DbUtils();
      return $dbutils->getUserName($id, 1);
   }

}
