<?php
namespace VdmPackage\services\helpers;

//use \retl\system\libraries\Paths;
//use \retl\system\libraries\GenericHtmlConverter;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Helper functions for URLs.
 *
 */
class UrlHelper
{
   /**
    * Given a key-value map, append a query string to a URL.
    *
    * @param   string   $url
    * @param   array    $qsmap   (Optional) A map of key-value
    * @param   bool     $encode  (Optional) Url encode $qsmap data (dft: TRUE)
    *
    * @todo Handle case when the url already contains query parameters.
    */
   public static function appendQueryParameters(
      string $url,
      array $qsmap = null, //Allow using this fct generically.
      bool $encode = true
   ) {
      //Nothing to do when not adding query string parameters
      if(empty($qsmap)) {
         return $url;
      }

      //Todo: Must handle case when url already contains query parameters.
      $url = $url.'?';

      //Create our part of the query string
      $qs = [];
      foreach($qsmap as $k => $v) {
         if($encode) {
            $k = urlencode($k);
            $v = urlencode($v);
         }
         $qs[] = $k.'='.$v;
      }
      $qs = implode('&', $qs);

      return $url.$qs;
   }

   /**
    * Tell if a url is absolute
    * An absolute URL begins with a protocol or //
    *
    * @param   string   $url
    * @retval  bool  TRUE if URL is absolute
    * @src  https://stackoverflow.com/a/19709846
    */
   public static function isAbsolute(string $url) {
      return (preg_match('#^(?:[a-z]+:)?//#i', trim($url)) === 1);
   }


   /**
    * Capitalise last segment of the URL
    * Given: abc/cde/fgh
    * Output: abc/cde/Fgh
    *
    * @param   string   $url
    * @return  string
    */
   public static function ucfirstLastSegment(string $url) {
      if(strlen($url) < 1) {
         return $url;
      }

      $segments = explode('/', $url);
      $n = count($segments);
      $segments[$n-1] = ucfirst($segments[$n-1]);
      return implode('/', $segments);
   }

   /**
    * Test method ucfirstLastSegment
    *
    * @return  void
    */
   public static function test_ucfirstLastSegment() {
      $test_cases = [
         'abc/cde/fgh' => 'abc/cde/Fgh',
         'abc/cde/Fgh' => 'abc/cde/Fgh',
         'abc/CDe/fGh' => 'abc/CDe/FGh',
         'ABC/CDE/FGH' => 'ABC/CDE/FGH',
         'abc' => 'Abc',
         '' => '',
      ];

      foreach($test_cases as $input => $expected) {
         $result = self::ucfirstLastSegment($input);
         if($result !== $expected) {
            throw new Exception($result.' !== '.$expected);
         }
      }
      echo 'Succes: '.__METHOD__;
   }


   /**
    * Tell if a url is absolute
    * An absolute URL begins with a protocol or //
    *
    * @param   string   $url
    * @retval  bool  TRUE if URL is absolute
    */
   public static function test_isAbsolute() {
      $urls = [
         'http://example.com' => true, // true - regular http absolute URL
         'HTTP://EXAMPLE.COM' => true, // true - HTTP upper-case absolute URL
         'https://www.exmaple.com' => true, // true - secure http absolute URL
         'ftp://example.com/file.txt' => true, // true - file transfer absolute URL
         '//cdn.example.com/lib.js' => true, // true - protocol-relative absolute URL
         '/myfolder/test.txt' => false, // false - relative URL
         'myfolder/test.txt' => false, // false - relative URL
         'test' => false,
         '  http://example.com  ' => true, // true - regular http absolute URL
         ' HTTP://EXAMPLE.COM ' => true, // true - HTTP upper-case absolute URL
         '  https://www.exmaple.com  ' => true, // true - secure http absolute URL
         ' ftp://example.com/file.txt' => true, // true - file transfer absolute URL
         '  //cdn.example.com/lib.js  ' => true, // true - protocol-relative absolute URL
         ' /myfolder/test.txt ' => false, // false - relative URL
         '  myfolder/test.txt  ' => false, // false - relative URL
         ' test' => false,
         '' => false,
      ];

      foreach($urls as $url => $expected) {
         $result = self::isAbsolute($url);
         if($expected !== $result) {
            $msg = $expected
               ? '(Bug) This URL is wrongfully marked as relative: '
               : '(Bug) This URL is wrongfully marked as absolute: ';
            throw new \Exception($msg.$url);
         }
      }
   }

}
