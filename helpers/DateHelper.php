<?php
namespace VdmPackage\services\helpers;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Helper functions for dates.
 */
class DateHelper
{
   /**
    * Check that a date string matches given date format.
    * Src: https://stackoverflow.com/a/19271434
    *
    * @param   string   $date    The date string
    * @param   string   $format  Desired PHP date format
    *
    * @return bool   TRUE if format matches, FALSE otherwise
    */
   public static function validateFormat(string $date, string $format) {
      $d = \DateTime::createFromFormat($format, $date);
      // Did we get a data object AND do string representation match after
      // auto corrections (such as converting 2019-02-30 to 2019-03-02)?
      return $d && $d->format($format) === $date;
   }
}
