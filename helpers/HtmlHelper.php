<?php
namespace VdmPackage\services\helpers;

//use \retl\system\libraries\Paths;
//use \retl\system\libraries\GenericHtmlConverter;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Helper functions for writing HTML.
 *
 */
class HtmlHelper
{

   /**
   * Prepare a string of attributes to insert into an HTML tag declaration
   * For example, tag attributes such as: id="..." class="..." data-foo="..."
   *
   * @param   array  $attributes (Optional) A map of button tag attributes
   * @param   array  $default_escapable_attributes (Optional) Default attributes
   *                             that can be HTML escaped.
   * @param   array  $default_unescapable_attributes (Optional) Default
   *                             attributes that can't be escaped.
   *                             Must already be escaped.
   * @param   bool   $to_string  (Optional) TRUE (dft): Return as string
   *                             FALSE: Return an array
   * @param   callable  $escape_fct_keys     (Optional) A callable for
   *                             escaping keys. Dft: htmlspecialchars
   * @param   callable  $escape_fct_values   (Optional) A callable for
   *                             escaping values. Dft: htmlspecialchars
   *
   * @return  string   A string of HTML tag attributes.
   */
   public static function prepareTagAttributes(
      array $attributes = [],
      array $default_escapable_attributes = [],
      array $default_unescapable_attributes = [],
      bool  $to_string = true,
      string $escape_fct_keys = "htmlspecialchars",
      string $escape_fct_values = "htmlspecialchars"
   ) {
      //Add default values that can be escaped
      $attributes = array_replace($default_escapable_attributes, $attributes);

      //Escape keys
      $attributes = ArrayHelper::encodeKeys($attributes, $escape_fct_keys);
      $default_unescapable_attributes = ArrayHelper::encodeKeys(
         $default_unescapable_attributes,
         $escape_fct_keys
      );

      //Escape values
      $attributes = array_map($escape_fct_values, $attributes);

      //Add unescapable attributes
      $attributes = array_replace($attributes, $default_unescapable_attributes);

      //Maybe, convert attribute map to string
      return $to_string
         ? ArrayHelper::implodeHtmlAttributeMap($attributes)
         : $attributes;
   }
}
