<?php
namespace VdmPackage\services\helpers;

//use \retl\system\libraries\Paths;
//use \retl\system\libraries\GenericHtmlConverter;

/*
 -------------------------------------------------------------------------
 Vdmtabs: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   Vdmtabs
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/Vdmtabs
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Helper functions for arrays.
 */
class ArrayHelper
{
   /**
    * Apply a callback to keys of an array
    *
    * @param   array    $map        A map of key-value
    * @param   Callable $callable   The encoding function:
    *                               str/numerical => str/numerical
    *
    * @return the modified array
    */
   public static function encodeKeys(array $map, Callable $callback) {
      $retval = [];
      foreach($map as $k => $v) {
         $retval[$callback($k)] = $v;
      }
      return $retval;
   }

   /**
    * Convert a map into a string
    *
    * @param   array    $map        A map of key-value
    * @param   string   $kv_glue    (Opptional) The string that joins
    *                               a key and it's value. Dft: '='
    * @param   string   $k_prefix   (Optional) A string to prepend to each key
    * @param   string   $k_suffix   (Optional) A string to append to each key
    * @param   string   $v_prefix   (Optional) A string to prepend to each value
    * @param   string   $v_suffix   (Optional) A string to append to each value
    *
    * @return string the imploded map
    */
   public static function implodeMap(
      array $map,
      string $kv_glue = '=',
      string $k_prefix = '',
      string $k_suffix = '',
      string $v_prefix = '',
      string $v_suffix = ''
   ) {
      $retval = [];
      foreach($map as $k => $v) {
         $kstr = $k_prefix.$k.$k_suffix;
         $vstr = $v_prefix.$v.$v_suffix;
         $retval[] = $kstr.$kv_glue.$vstr;
      }
      return implode('', $retval);
   }

   /**
    * Convert a map into a string of HTML tag attributes
    *
    * @param   array    $map        A map of key-value
    *
    * @return string the imploded map
    */
   public static function implodeHtmlAttributeMap(array $map) {
      return self::implodeMap(
         $map,
         '=',
         '',
         '',
         '"',
         '"'
      );
   }

   /**
    * Recursively unset all array keys associated to an empty string
    *
    * @param   array    $data
    * @return  array    The modified array
    */
   public static function recursivelyUnsetEmptyStrings(array $data) {
      foreach($data as $k => &$v) {
         if(is_array($v)) {
            $v = self::recursivelyUnsetEmptyStrings($v);
         } elseif(is_string($v) && strlen($v) < 1) {
            unset($data[$k]);
         }
      }
      unset($v); //Prevent reference related bugs
      return $data;
   }

   /**
    * Unset all array keys associated to an empty string
    *
    * @param   array    $data
    * @return  array    The modified array
    */
   public static function unsetEmptyStrings(array $data) {
      foreach($data as $k => $v) {
         if(is_string($v) && strlen($v) < 1) {
            unset($data[$k]);
         }
      }
      return $data;
   }

   /**
    * Insert a value at before given position of an existing array.
    * Note: Reindexes numerical keys, preserves string keys.
    *
    * See: https://stackoverflow.com/a/11321318
    * See: https://stackoverflow.com/a/3353956
    *
    * Given:
    *    $main = [
    *       0	=>	'PHP code tester Sandbox Online',
    *       'foo'	=>	'bar',
    *       4	=>	15,
    *       5	=>	89009,
    *       'case'	=>	'Random Stuff: 300',
    *       'PHP Version'	=>	'7.2.4',
    *       6	=>	'bar',
    *    ];
    *
    *    $sub = [
    *       0	=>	'sub value 1',
    *       'buzz'	=>	'sub value 2'
    *       4	=>	15,
    *       'zzub'	=>	'7.2.4',
    *       6	=>	'sub value 5',
    *    ];
    *
    *    with $index = 5, return:
    *    [
    *       0	=>	'PHP code tester Sandbox Online',
    *       'foo'	=>	'bar',
    *       4	=>	15,
    *       5	=>	'sub value 1', //Numerical index 0 ajusted to 5
    *       'buzz'	=>	'sub value 2'
    *       6	=>	15, //Numerical index 4 ajusted to 6
    *       'zzub'	=>	'7.2.4',
    *       7	=>	'sub value 5', //Numerical index 6 ajusted to 7
    *       8	=>	89009, //Numerical index 5 ajusted to 8
    *       'case'	=>	'Random Stuff: 300',
    *       'PHP Version'	=>	'7.2.4',
    *       9	=>	'bar', //Numerical index 6 ajusted to 9
    *    ];
    *
    *    with $index = 'case', return:
    *    [
    *       0	=>	'PHP code tester Sandbox Online',
    *       'foo'	=>	'bar',
    *       4	=>	15,
    *       5	=>	89009,
    *       6	=>	'sub value 1', //Numerical index 0 ajusted to 6
    *       'buzz'	=>	'sub value 2'
    *       7	=>	15, //Numerical index 4 ajusted to 7
    *       'zzub'	=>	'7.2.4',
    *       8	=>	'sub value 5', //Numerical index 6 ajusted to 8
    *       'case'	=>	'Random Stuff: 300',
    *       'PHP Version'	=>	'7.2.4',
    *       9	=>	'bar', //Numerical index 6 ajusted to 9
    *    ];
    *
    * @param   array       $main    The array to modifiy
    * @param   int/string  $index   The index before which to insert sub-array
    * @param   array/mixed $sub     The array to insert
    *                               (Non-arrays are wrapped in an array)
    * @return  array The modified array
    */
   public static function insertBefore(array $main, $index, $sub)
   {
      $size = count($main);

      if(
         (is_string($index) && ! isset($main[$index]))
         || (is_numeric($index) && ($index < 0 || $index > $size))
         || ! (is_string($index) || is_numeric($index))
      )
      {
         throw new \Exception(
            'Usage: Index must be a string or integer.'
            .' Strings must be a key in $main.'
            .' Integers must be in-bounds of $main size.'
         );
      }

      //Automatic type conversion to array
      $sub = is_array($sub) ? $sub : [$sub];

      //Sub array must not share keys of type string with main array
      //to avoid overwriting values by accident.
      foreach($sub as $k => $v) {
         if(is_string($k) && isset($main[$k])) {
            throw new \Exception(
               sprintf(
                  '%s %s %s',
                  'An array value may be lost while inserting the sub-array.',
                  'Main and sub arrays share the following key:',
                  $k
               )
            );
         }
      }

      $retval = [];

      //Edge case: Empty main with index = 0;
      if(count($main) < 1) {
         return $sub;
      }

      $i = -1;
      foreach($main as $k => $v) {
         //Reproduce PHP's automatic array indexing
         if(is_int($k) && $i < $k) {
            $i = $k;
         }

         //Should we insert sub array here?
         if($k === $index) {
            foreach($sub as $sk => $sv) {
               //This preserves string keys and increments integers keys
               if( ! is_string($sk)) {
                  //Sub array key index replaced with main array
                  //numerical indexing
                  $retval[$i++] = $sv;
               } else {
                  $retval[$sk] = $sv;
               }
            }
         }

         //This preserves string keys and increments integers keys of $main
         if( ! is_string($k)) {
            //Original index potentially incremented
            $retval[$i++] = $v;
         } else {
            $retval[$k] = $v;
         }
      }
      return $retval;
   }

//-----------------------------------------------------------------------------
// Tests
//-----------------------------------------------------------------------------

   public static function test_insertBefore() {

       $main = [
          0	=>	'PHP code tester Sandbox Online',
          'foo'	=>	'bar',
          4	=>	15,
          5	=>	89009,
          'case'	=>	'Random Stuff: 300',
          'PHP Version'	=>	'7.2.4',
          6	=>	'bar',
       ];

       $sub = [
          0	=>	'sub value 1',
          'buzz'	=>	'sub value 2',
          4	=>	15,
          'zzub'	=>	'7.2.4',
          6	=>	'sub value 5',
       ];
       $tests = [];

       $test = [];
       $test['main'] = $main;
       $test['sub'] = $sub;
       $test['index'] = 5;
       $test['expected'] = [
          0	=>	'PHP code tester Sandbox Online',
          'foo'	=>	'bar',
          4	=>	15,
          5	=>	'sub value 1', //Numerical index 0 ajusted to 5
          'buzz'	=>	'sub value 2',
          6	=>	15, //Numerical index 4 ajusted to 6
          'zzub'	=>	'7.2.4',
          7	=>	'sub value 5', //Numerical index 6 ajusted to 7
          8	=>	89009, //Numerical index 5 ajusted to 8
          'case'	=>	'Random Stuff: 300',
          'PHP Version'	=>	'7.2.4',
          9	=>	'bar', //Numerical index 6 ajusted to 9
       ];
       $tests[] = $test;

       $test = [];
       $test['main'] = $main;
       $test['sub'] = $sub;
       $test['index'] = 'case';
       $test['expected'] = [
          0	=>	'PHP code tester Sandbox Online',
          'foo'	=>	'bar',
          4	=>	15,
          5	=>	89009,
          6	=>	'sub value 1', //Numerical index 0 ajusted to 6
          'buzz'	=>	'sub value 2',
          7	=>	15, //Numerical index 4 ajusted to 7
          'zzub'	=>	'7.2.4',
          8	=>	'sub value 5', //Numerical index 6 ajusted to 8
          'case'	=>	'Random Stuff: 300',
          'PHP Version'	=>	'7.2.4',
          9	=>	'bar', //Numerical index 6 ajusted to 9
       ];
       $tests[] = $test;

       foreach($tests as $test_index => $test) {
          $result = self::insertBefore(
             $test['main'],
             $test['index'],
             $test['sub']
          );

          echo '<table>';
          echo '<tr>';
            echo '<th>Result</th>';
            echo '<th>Expected</th>';
            echo '<th>Main</th>';
            echo '<th>Sub</th>';
            echo '<th>Index</th>';
          echo '<tr>';
          echo '</tr>';
          echo '<tr>';
             echo '<td><pre>', var_export($result, true), '</pre></td>';
             echo '<td><pre>', var_export($test['expected'], true), '</pre></td>';
             echo '<td><pre>', var_export($test['main'], true), '</pre></td>';
             echo '<td><pre>', var_export($test['sub'], true), '</pre></td>';
             echo '<td><pre>', var_export($test['index'], true), '</pre></td>';
          echo '</tr>';
          echo '</table>';

          //Compare key-value pairs
          foreach($test['expected'] as $k => $v) {
             if( ! isset($result[$k]) OR $result[$k] !== $v) {
                throw new \Exception(sprintf(
                   'Invalid result for (test index; main key): (%d; %s)',
                   $test_index,
                   $k
                ));
             }
          }

          //Compare key-value pairs in the other direction
          foreach($result as $k => $v) {
             if( ! isset($test['expected'][$k])
                  OR $test['expected'][$k] !== $v) {
                throw new \Exception(sprintf(
                   'Invalid result for (test index; sub key): (%d; %s)',
                   $test_index,
                   $k
                ));
             }
          }

          if(count($result) != count($test['expected'])) {
             throw new \Exception('Array count mismatch for test index: '.$test_index);
          }

          //Compare key ordering
          $expected_keys = array_keys($test['expected']);
          $result_keys = array_keys($result);
          for($j = 0; $j < count($expected_keys) && $j < count($result_keys); $j++) {
             if($expected_keys[$j] !== $result_keys[$j]) {
                throw new \Exception(sprintf(
                   '%s (%d; %s; %s)',
                   'Invalid key ordering for (test index; main key; sub key):',
                   $test_index,
                   $expected_keys[$j],
                   $result_keys[$j]
                ));
             }
          }
       }
   }


}
