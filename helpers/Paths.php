<?php
namespace VdmPackage\services\helpers;

/*
 -------------------------------------------------------------------------
 RETL library for reversible extract-transform-load data migration operations
 --------------------------------------------------------------------------
 @package   retl
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/retl
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Librairy of helper methods related to paths and the file system.
 * Should only contain static methods and class constants.
 */
class Paths
{
    /**
    * Remove double separators and extra spaces
    *
    * DOES:
    *    - Remove leading and trailing separator and spaces from each segment.
    *    - Keep leading separator at the begining of first segment.
    *    - Keep trailing separator at end of last segment.
    *    - Uses PHP constant DIRECTORY_SEPARATOR by default.
    *
    * DOESN'T:
    *    - Validate path syntax or correctness
    *    - Validate path existence.
    *
    * @param   string   $path
    * @return  string   The cleaned path
    */
   public static function clean(string $path, string $separator = ''): string {
      if(strlen($separator) < 1) {
         $separator = DIRECTORY_SEPARATOR;
      }

      $path = trim($path);

      //Edge case: Keep it short
      if($path === $separator) {
         return $separator;
      }

      //Edge case: Backslash must be escaped twice. Once for PHP, once for regex
      $sep = $separator == '\\' ? '\\\\' : $separator;

      //Build a regex for anything that must be replaced with a single separator
      $regex = [];

      //Open regex
      $regex[] = '#';
         //Preceeded by zero or more separator and/or space
         $regex[] = '['.$sep.'\s]*';
         //Find a separator
         $regex[] = '['.$sep.']';
         //Followed by zero or more separator and/or space
         $regex[] = '['.$sep.'\s]*';
      //Close regex and set case insensitive flag
      $regex[] = '#i';
      //Regex must be a string
      $regex = implode('', $regex);

      $path = preg_replace(
         $regex,
         $separator,
         $path
      );

      return is_string($path) ? $path : '';
   }

    /**
    * Concatenate a list of path segments into a clean path string.
    * Rem: Uses method self::clean
    *
    * @param array  $segments  A list of path segments from parent to child.
    * @return string The imploded path
    */
   public static function implodePath(array $segments, string $separator = '') {
      if(strlen($separator) < 1) {
         $separator = DIRECTORY_SEPARATOR;
      }

      //Detect presence of leading and trailing separators
      $path = implode('', $segments);
      $path = trim($path);
      $has_leading_separator = (isset($path[0]) && $path[0] == $separator);
      $n = strlen($path)-1;
      $has_trailing_separator = (isset($path[$n]) && $path[$n] == $separator);

      //Implode path and clean extra spaces and separators
      $path = self::clean(
         implode($separator, $segments),
         $separator
      );

      //Restore leading separator state
      $path = $has_leading_separator
         ? $separator.ltrim($path, $separator)
         : ltrim($path, $separator);

      //Restore trailing separator state
      $path = $has_trailing_separator
         ? rtrim($path, $separator).$separator
         : rtrim($path, $separator);

      return $path;
   }

    /**
    * Create a directory only if missing. Die on error.
    *
    * @param string  $path  An absolute path to the directory to create, if missing
    * @return void
    */
   public static function makeDirOrDie($path) {
      @ mkdir($path)
      or die("Can't create directory: ".$path);
   }

//-----------------------------------------------------------------------------
// Tests
//-----------------------------------------------------------------------------

    /**
    * Test method.
    *
    * @return void
    * @throws Exceptions
    */
   public static function test() {
      return date('Y-m-d H:i:s');
   }

    /**
    * Test method.
    *
    * @return void
    * @throws Exceptions
    */
   public static function test_implodePath() {
      $tests = [
         [
            'separator' => '/',
            'segments' => [
               '',
            ],
            'expected' => '',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/',
            ],
            'expected' => '/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               '/plugins/vdmseram/front',
               '/abc/def/ghi/',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               '/plugins/vdmseram/front',
               '/abc/def/ghi/',
               '',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/',
               '/plugins/vdmseram/front',
               '/abc/def/ghi/',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/plugins/vdmseram/front',
               '/abc/def/ghi/',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               'plugins/vdmseram/front',
               '/abc/def/ghi/',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               'plugins/vdmseram/front',
               '/abc/def/ghi',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               'plugins/vdmseram/front',
               '/abc/def/ghi/',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               'plugins/vdmseram/front',
               '/abc/def/ghi',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               'plugins/vdmseram/front',
               '/abc/def/ghi',
               '',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               'plugins/vdmseram/front',
               '/abc/def/ghi',
               '/',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '',
               'plugins/vdmseram/front',
               '/abc/def/ghi/',
               '/',
            ],
            'expected' => 'plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/',
               'plugins/vdmseram/front',
               '/abc/def/ghi/',
               '/',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/',
               '/plugins/vdmseram/front',
               '/abc/def/ghi/',
               '//',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '/',
            'segments' => [
               '/ /',
               '//plugins///vdmseram//front///',
               '////abc//def///ghi///',
               '////',
            ],
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'separator' => '\\',
            'segments' => [
               '\\',
               '\\plugins\\vdmseram\\\\front\\',
               '\\abc\\\\def\\ghi',
               '\\',
            ],
            'expected' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi\\',
         ],
      ];

      foreach($tests as $i => $test) {
         $result = self::implodePath($test['segments'], $test['separator']);
         if($test['expected'] !== $result) {
            $test['index'] = $i;
            $test['result'] = $result;
            throw new \Exception(
               'Incorrect result for: '.PHP_EOL.var_export($test, true)
            );
         }
      }
   }

   public static function test_clean() {
      $tests = [
         [
            'path' => '',
            'separator' => '\\',
            'expected' => '',
         ],
         [
            'path' => '\\',
            'separator' => '\\',
            'expected' => '\\',
         ],
         [
            'path' => '\\',
            'separator' => '/',
            'expected' => '\\',
         ],
         [
            'path' => '/',
            'separator' => '\\',
            'expected' => '/',
         ],
         [
            'path' => '/',
            'separator' => '/',
            'expected' => '/',
         ],
         [
            'path' => '/',
            'separator' => '\\',
            'expected' => '/',
         ],
         [
            'path' => '\\\\\\',
            'separator' => '\\',
            'expected' => '\\',
         ],
         [
            'path' => '//////',
            'separator' => '\\',
            'expected' => '//////',
         ],
         [
            'path' => '///',
            'separator' => '/',
            'expected' => '/',
         ],
         [
            'path' => '///////',
            'separator' => '/',
            'expected' => '/',
         ],
         [
            'path' => '      / / /      ////    ',
            'separator' => '/',
            'expected' => '/',
         ],
         [
            'path' => '      \\ \\ \\      \\\\\\\\    ',
            'separator' => '\\',
            'expected' => '\\',
         ],
         [
            'path' => '      / / /      ////    ',
            'separator' => '\\',
            'expected' => '/ / /      ////',
         ],
         [
            'path' => '      \\ \\ \\      \\\\\\    ',
            'separator' => '/',
            'expected' => '\\ \\ \\      \\\\\\',
         ],
         [
            'path' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi\\',
            'separator' => '\\',
            'expected' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi\\',
         ],
         [
            'path' => '/plugins/vdmseram/front/abc/def/ghi/',
            'separator' => '/',
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'path' => 'plugins\\vdmseram\\front\\abc\\def\\ghi\\',
            'separator' => '\\',
            'expected' => 'plugins\\vdmseram\\front\\abc\\def\\ghi\\',
         ],
         [
            'path' => 'plugins/vdmseram/front/abc/def/ghi/',
            'separator' => '/',
            'expected' => 'plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'path' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi',
            'separator' => '\\',
            'expected' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi',
         ],
         [
            'path' => '/plugins/vdmseram/front/abc/def/ghi',
            'separator' => '/',
            'expected' => '/plugins/vdmseram/front/abc/def/ghi',
         ],
         [
            'path' => '     \\plugins    \\ \\   \\\\\\ vdmseram\\front\\abc\\def\\ghi\\      \\          ',
            'separator' => '\\',
            'expected' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi\\',
         ],
         [
            'path' => '  /    plugins     /     /// vdmseram /front////abc/def/ghi      //  ////        ',
            'separator' => '/',
            'expected' => '/plugins/vdmseram/front/abc/def/ghi/',
         ],
         [
            'path' => '\\\\\\plugins\\vdmseram\\\\front\\\\\\abc\\\\def\\ghi\\\\',
            'separator' => '\\',
            'expected' => '\\plugins\\vdmseram\\front\\abc\\def\\ghi\\',
         ],
      ];

      foreach($tests as $i => $test) {
         $result = self::clean($test['path'], $test['separator']);
         if($test['expected'] !== $result) {
            $test['index'] = $i;
            $test['result'] = $result;
            throw new \Exception(
               'Incorrect result for: '.PHP_EOL.var_export($test, true)
            );
         }
      }
   }

}
