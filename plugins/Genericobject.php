<?php
namespace VdmPackage\services\plugins;

use VdmPackage\services\glpi\ItemtypeDAO;

/*
 -------------------------------------------------------------------------
 vdmseram: Custom GUI for business process workflows
 --------------------------------------------------------------------------
 @package   vdmseram
 @author    Ville de Montréal
 @link      https://github.com/VilledeMontreal/vdmseram
 @link      http://www.glpi-project.org/
 @since     2018
 --------------------------------------------------------------------------
*/
/**
 * Service for interacting with GLPI users
 *
 */
class Genericobject
{
   /*
    * Get the frontend label of a field, as GenericObject would display it.
    *
    * @param   string   $field_name    The field code name
    * @param   string   $itemtype      The GLPI itemtype (full class name)
    * @param   mixed    $dft           (Optional) The value on failure
    *                                  Defaults to empty string
    * @return  string   The field label or $dft
   */
   public static function getFieldLabel(
      string $field_name,
      string $itemtype,
      $dft = NULL
   ) {
      //Avoid array index not set errors from function "getFieldOptions"
      if( ! ItemtypeDAO::isField($field_name, $itemtype)) {
         return $dft;
      }

      //Get the field definition from ".constant.php" config file
      $options = \PluginGenericobjectField::getFieldOptions(
         $field_name,
         $itemtype
      );

      //Get display name
      return (
            is_array($options)
            && isset($options['name'])
            && is_string($options['name'])
         )
         ? $options['name']
         : $dft;
   }

   /**
    * Reads from global scoped array GO_FIELDS
    * and returns wanted attribute value
    *
    * GO_FIELDS is an array feeded by php files in folder :files\_plugins\genericobject\fields\
    * The php files that feed this array are like : field.constant.php and bda.constant.php
    *
    * GO_FIELDS contains for each field of an item object the following attributes as example:
    *
    * Example 1 :
    *    $GO_FIELDS['actions_id']['name']          = "Opération";
    *    $GO_FIELDS['actions_id']['input_type']    = 'dropdown';
    *    $GO_FIELDS['actions_id']['entities_id']      = true;
    *    $GO_FIELDS['actions_id']['is_recursive']     = true;
    * ----------------------------------------------------------------------
    * Example 2 :
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['name']             = "Statut IC (Régularisé) - BDA";
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['field']            = 'modelpuissbda';
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['input_type']       = 'dropdown';
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['entities_id']      = true;
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['is_recursive']     = true;
    * $GO_FIELDS['plugin_genericobject_statuticbdas_id']['is_tree']          = false;
    *
    * Input of this function is like :
    *    $value = 'actions_id'
    *    $attr  = 'name'
    * The output should be then : "Opération"
    *
    *
    * @return  scalar value.
    *
    */
   public static function getGOFields( $value, $attr ){

        global $GO_FIELDS;

        if( ! isset($attr) ){
            return 'null';
        }

        if( isset($GO_FIELDS[ $value ][$attr]) ){
            return substr($GO_FIELDS[ $value ][$attr], 0, 40);
        } else {
            $pattern = '/plugin_genericobject_(.+)/';
            $replacement = '${1}';
            $short_value = preg_replace($pattern, $replacement, $value);

            if( isset($GO_FIELDS[ $short_value ][$attr]) ){
                return substr($GO_FIELDS[ $short_value ][$attr], 0, 40);
            }
        }

        if( $attr != 'name' ){
            return 'null';
        } else {
            return $value;
        }

   }

   /*
   * Wrapper for legacy code
   * Use the correct function name instead.
   */
   public static function get_go_fields( $value, $attr ){
      self::getGOFields($value, $attr);
   }


    /**
    * Generates HTML of simple [select] Tag
    *
    *
    * @return  HTML.
    *
    */
   public static function generateDropDown( $arg_arr ) {
      global $DB, $GO_FIELDS;

      if( preg_match('/glpi_alls/', $arg_arr['where'], $matches ) &&
          preg_match('/COLUMNS/i', $arg_arr['table_name'], $matches )
      ){
           // We are in objectless rule (rule will affect all objects)

           unset( $arg_arr['where'] );
           unset( $arg_arr['table_name'] );
           unset( $arg_arr['key_name'] );
           unset( $arg_arr['value_name'] );

           $arg_arr['input_array'] = [
                'id'                  =>    self::get_go_fields('id', 'name'),
                'is_template'         =>    self::get_go_fields('is_template', 'name'),
                'template_name'       =>    self::get_go_fields('template_name', 'name'),
                'is_deleted'          =>    self::get_go_fields('is_deleted', 'name'),
                'entities_id'         =>    self::get_go_fields('entities_id', 'name'),
                'is_recursive'        =>    self::get_go_fields('is_recursive', 'name'),
                'name'                =>    self::get_go_fields('name', 'name'),
                'serial'              =>    self::get_go_fields('serial', 'name'),
                'otherserial'         =>    self::get_go_fields('otherserial', 'name'),
                'locations_id'        =>    self::get_go_fields('locations_id', 'name'),
                'states_id'           =>    self::get_go_fields('states_id', 'name'),
                'users_id'            =>    self::get_go_fields('users_id', 'name'),
                'groups_id'           =>    self::get_go_fields('groups_id', 'name'),
                'manufacturers_id'    =>    self::get_go_fields('manufacturers_id', 'name'),
                'users_id_tech'       =>    self::get_go_fields('users_id_tech', 'name'),
                'comment'             =>    self::get_go_fields('comment', 'name'),
                'is_helpdesk_visible' =>    self::get_go_fields('is_helpdesk_visible', 'name'),
                'notepad'             =>    self::get_go_fields('notepad', 'name'),
                'date_mod'            =>    self::get_go_fields('date_mod', 'name'),
                'date_creation'       =>    self::get_go_fields('date_creation', 'name'),
                'codeplug'            =>    self::get_go_fields('codeplug', 'name'),
            ];

           return self::generateDropDownArr( $arg_arr );
      }

      $events = "";
      if( isset( $arg_arr['events'] ) ){
          foreach ( $arg_arr['events'] as $evt => $fct ){
               $events .= sprintf('%s="%s" ', $evt, $fct);
          }
      }

      if( isset($arg_arr['column_code']) ){
            if( preg_match('/^(\w+)_id$/', $arg_arr['column_code'], $matches ) ){
                $arg_arr['table_name'] = "glpi_" . $matches[1];
            } else if ( preg_match('/^(plugin_genericobject_\w+)_id_(\w+)_id$/', $arg_arr['column_code'], $matches ) ){
                $arg_arr['table_name'] = "glpi_" . $matches[1];
            } else {
                $arg_arr['table_name'] =  "INFORMATION_SCHEMA.TABLES";
                $arg_arr['where'] =  "1=2";
            }
      }


      $html_result = sprintf('<select name="%s" id="%s"  %s>', $arg_arr['id_select'], $arg_arr['id_select'], $events );
      $sql = "";


      if( isset( $arg_arr['prepend_options'] ) ){
          foreach ( $arg_arr['prepend_options'] as $value => $label ){
               $html_result .= sprintf('<option value="%s">%s</option>', $value, $label);
          }
      }


      $sql = sprintf( '
              SELECT distinct %s as id, %s as name
              FROM  %s
              where 1=1
              and %s
              %s',
                        $arg_arr['key_name'], $arg_arr['value_name'],
                        $arg_arr['table_name'], $arg_arr['where'],
                        $arg_arr['order']
            );


      $iterator = $DB->request($sql);

      $arr_results = [];
      while($ligne = $iterator->next()){
         $arr_results[$ligne['id']] = self::get_go_fields($ligne['name'], 'name');
      }


     foreach ( $arr_results as $key => $value) {
        if( $arg_arr['selected_key'] == $key . "" ){
           $selected = 'selected';
        } else {
            $selected = '';
        }


        if( self::get_go_fields($key, 'input_type') == 'null' ){
            $input_type = 'text';
        } else {
            $input_type = self::get_go_fields($key, 'input_type');
        }

        $html_result .= "<option $selected input_type='$input_type' value='" . $key . "'>" . $value . "</option>";
     }

      $html_result .= "</select>";
      return $html_result;
   }

   /*
   * Wrapper for legacy code
   * Use the correct function name instead.
   */
   public static function generate_DropDown( $arg_arr ) {
      return self::generateDropDown($arg_arr);
   }


    /**
    * Generates HTML of simple [select] Tag
    * from an array passed in params
    *
    * @return  HTML.
    *
    */
   public static function generateDropDownArr( $arg_arr ) {
      global $DB, $GO_FIELDS;

      $events = "";
      if( isset( $arg_arr['events'] ) ){
          foreach ( $arg_arr['events'] as $evt => $fct ){
               $events .= sprintf('%s="%s" ', $evt, $fct);
          }
      }


      $html_result = sprintf('<select name="%s" id="%s"  %s>', $arg_arr['id_select'], $arg_arr['id_select'], $events );
      $sql = "";


      if( isset( $arg_arr['prepend_options'] ) ){
          foreach ( $arg_arr['prepend_options'] as $value => $label ){
               $html_result .= sprintf('<option value="%s">%s</option>', $value, $label);
          }
      }

     foreach ( $arg_arr['input_array'] as $key => $value) {
        if( $arg_arr['selected_key'] == $key . "" ){
           $selected = 'selected';
        } else {
            $selected = '';
        }


        if( self::get_go_fields($key, 'input_type') == 'null' ){
            $input_type = 'text';
        } else {
            $input_type = self::get_go_fields($key, 'input_type');
        }

        $html_result .= "<option $selected input_type='$input_type' value='" . $key . "'>" . $value . "</option>";
     }

      return $html_result;
   }

   /*
   * Wrapper for legacy code
   * Use the correct function name instead.
   */
   public static function generate_DropDown_arr( $arg_arr ) {
      return self::generateDropDownArr($arg_arr);
   }

}
