<?php
namespace VdmPackage\services\models\traits;

//use VdmPackage\errors\PublicException;

/**
 * Add field remapping functionnality to a GLPI itemtype.
 *
 * For example,
 *
 *    $item->gropcode_id automatically remaps
 *    to $item->fields['plugin_genericobject_groptdus_id_gropcodes_id']
 *
 * On top of magic methods, provide ways to interrogate field mappings.
 * Using inheritance instead of composition to simplify integration with GLPI.
 *
 * Expectations:
 *    1- isset($this->fields) and is_array($this->fields)
 *    2- Property "fields" is a map of field name to field value.
 *
 * Usage:
 *    1- Create a class that extends the class of a GLPI itemtype.
 *    2- Make your class use this trait.
 *    3- Make your class implement interface "ILogicalItemtype".
 *       This will make it possible to write generic functions without relying
 *       on inheritance and while also enforcing assumptions about
 *       LogicalItemtype objects.
 *    4- Use your class.
 *
 */
trait LogicalItemtypeTrait {

   /*
   * Convert a logical name into an actual field name or return $dft
   * If it's already a real name, it will be returned unchanged.
   *
   * @param   string    $name The field name to convert
   * @param   mixed     $dft  (Optionnal) Value to return if not found.
   * @return   string|''   The real name or an empty string
   */
   public function getFieldRealName(string $name, $dft = ''): string {
      $retval = $dft;
      //Historical case

      if(array_key_exists($name, $this->fields)) {
         $retval = $name;
      } elseif(//test generic accessor) {

      }

   }

   /*
   * Tell if there exists a real field for given logical name.
   *
   * @param   string    $name The field name to convert
   * @return   bool
   */
   public function hasFieldRealName(string $name): bool {
      return strlen($name) && $this->getFieldRealName($name, '') !== '';
   }

   /*
   * Generic accessor for field values that accepts logical names.
   *
   * @param   string    $name The field name to convert
   * @param   mixed     $dft  (Optionnal) Value to return if not found.
   * @return   string|''   The real name or an empty string
   */
   public function getValue(string $name, $dft = null) {
      $retval = $dft;
      $field = $this->getFieldRealName($name, '');

      return strlen($field) > 0 ? $this->fields[$field] : $retval;
   }

   /*
   * Generic accessor for field values that accepts logical names.
   *
   * @param   string    $name    The field name to convert
   * @param   mixed     $value   The value to set
   * @return   void
   */
   public function setValue(string $name, $value) {
      $retval = $dft;
      $field = $this->getFieldRealName($name, '');
   }

}
